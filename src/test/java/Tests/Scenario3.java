package Tests;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class Scenario3 {

    @Test
    public void updateFilterTest() {
        String bodyContent = "{" +
        "\"userId\": 1," +
        "\"id\": 1," +
        "\"title\": \"id labore ex et quam laborum\"," +
        //"\"email\": \"Eliseo@gardner.biz\"," +
        "\"body\": \"NEW comment\"}";

        ValidatableResponse response =
                given()
                .contentType(ContentType.JSON)
                .when()
                .body(bodyContent)
                .put("https://jsonplaceholder.typicode.com/posts/1")
                .then()
                .assertThat()
                .statusCode(204);

    }

}
