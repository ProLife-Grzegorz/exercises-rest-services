package Tests;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static java.util.Comparator.comparing;

public class Scenario1 {

    @Test
  public void FilterByUserIdTest() {

        ValidatableResponse response =
      given()
              .contentType(ContentType.JSON)
              .when()
              .get("https://jsonplaceholder.typicode.com/posts")
                .then()
              .contentType(ContentType.JSON);
              //.statusCode(200);
              //.body("userId",equalTo(10));

        String responseGet = response.extract().body().asString();
        //System.out.println(responseGet);

        List<String> list = new ArrayList<String>();
        list.add(responseGet);

        System.out.println(Collections.max(list,null));

    }
}
